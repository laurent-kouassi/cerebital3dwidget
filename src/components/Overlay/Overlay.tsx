import React from 'react'

const Overlay = () => {
    return (
        <h1 style={{ width: "100vw", color: "black", textAlign: "center" }}>
            3D Model Render Demo
        </h1>
    )
}

export default Overlay