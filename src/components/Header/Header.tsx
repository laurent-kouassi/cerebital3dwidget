import React, { useEffect, useState } from 'react'
import Search from './Search';
import { selectedObject } from '../../store/actions';
import { useDispatch, useSelector } from 'react-redux';
import Menubar from '../Menubar/Menubar';

export const objectsModelsData = [
    { 
        id: 1, 
        name: "car",
        uri: "/models/car.glb"
    },
    { 
        id: 2, 
        name: "damage helmet",
        uri: "/models/DamagedHelmet.glb"
    },
    { 
        id: 3, 
        name: "drill",
        uri: "/models/drill.glb"
    },
    { 
        id: 4, 
        name: "hammer",
        uri: "/models/hammer.glb"
    },
    { 
        id: 5, 
        name: "tape measure",
        uri: "/models/tapeMeasure.glb"
    },
];


const Header = () => {
    const selectedobject = useSelector((state: any) => state.ObjectReducer.selectedobject);
    const dispatch = useDispatch();

    const handleSelectedObject = (object: any) => {
        dispatch(
            selectedObject(
                object
            )
        );
    };
    
    // init first object select if not already selected
    useEffect(() => {
        if(!selectedobject){
            dispatch(
                selectedObject(
                    objectsModelsData[1]
                )
            ); 
        }
    }, [selectedobject]);
    
    return (
        <div className='flex flex-row  justify-between h-[68.78px] bg-[#FFFFFF] shadow-lg '>
            <div className='flex items-center px-4'>
                <Menubar />
            </div>
            <div className='flex flex-row justify-center items-center '>
                <Search
                    options={objectsModelsData}
                    label="name"
                    id="id"
                    selectedVal={selectedobject?.name}
                    handleChange={(obj: any) => handleSelectedObject(obj)} 
                />
            </div>
            <div></div>
        </div>
    )
};

export default Header;