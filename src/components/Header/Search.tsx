import React, { useEffect, useRef, useState } from 'react';

const Search = ({
    options,
    label,
    id,
    selectedVal,
    handleChange
}: any) => {
    const [query, setQuery] = useState("");
    const [isOpen, setIsOpen] = useState(false);

    const inputRef = useRef(null);

    useEffect(() => {
        document.addEventListener("click", toggle);
        return () => document.removeEventListener("click", toggle);
    }, []);

    const selectOption = (option: any) => {
        setQuery(() => "");
        handleChange(option);
        setIsOpen((isOpen) => !isOpen);
    };

    function toggle(e: any) {
        setIsOpen(e && e.target === inputRef.current);
    }

    const getDisplayValue = () => {
        if (query) return query;
        if (selectedVal) return selectedVal;

        return "";
    };

    const filter = (options: any) => {
        return options.filter(
            (option: any) => option[label].toLowerCase().indexOf(query.toLowerCase()) > -1
        );
    };

    return (
        <div className='dropdown'>
            <div className="control">
            <div className="selected-value relative">
                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg
                        aria-hidden="true"
                        className="w-5 h-5 text-gray-500 dark:text-gray-400"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                        >
                        </path>
                    </svg>
                </div>

                <input
                    ref={inputRef}
                    type="search"
                    id="default-search"
                    value={getDisplayValue()}
                    onChange={(e) => {
                        setQuery(e.target.value);
                        handleChange(null);
                    }}
                    name="subcribe_name"
                    placeholder="Search"
                    className="text-black placeholder-[#AFAFAF] placeholder:text-[12.5057px] placeholder:leading-[125%] min-w-[301.18px] h-[40px] text-base transition duration-500 ease-in-out transform border-transparent rounded-lg bg-gray-200  focus:border-blueGray-500 focus:bg-white dark:focus:bg-gray-800 focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2 ring-gray-400"
                />
                </div>
            </div>
            <div className={`options ${isOpen ? "open" : ""}`}>
                {filter(options).map((option: any, index: number) => {
                    return (
                        <div
                            onClick={() => selectOption(option)}
                            className={`option ${option[label] === selectedVal ? "selected" : "" }`}
                            key={`${id}-${index}`}
                        >
                            {option[label]}
                        </div>
                    );
                })}
            </div>
        </div>
    )
};

export default Search;