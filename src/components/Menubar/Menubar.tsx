import React from 'react';
import { FaRotate } from 'react-icons/fa6';
import { CiZoomIn, CiZoomOut } from "react-icons/ci";

const Menubar = () => {
  return (
    <div className='flex flex-row space-x-4'>
        <span className='text-black cursor-pointer'>
            <FaRotate />
        </span>
        <span className='text-black cursor-pointer text-[20px]'>
            <CiZoomIn />
        </span>
        <span className='text-black cursor-pointer text-[20px]'>
            <CiZoomOut />
        </span>
    </div>
  )
};

export default Menubar;