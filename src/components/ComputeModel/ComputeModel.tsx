import React from 'react';
import { useGLTF } from "@react-three/drei";
import { useSelector } from 'react-redux';
import { objectsModelsData } from '../Header/Header';

const ComputeModel = () => {
    const selectedobject = useSelector((state: any) => state.ObjectReducer.selectedobject);
    const objectUri = selectedobject?.uri ?? objectsModelsData[1]?.uri;

    const computer: any = useGLTF(objectUri);

    console.log({computer});
    return (
        <mesh>
            <hemisphereLight intensity={0.15} groundColor="black" />
            <spotLight
                position={[-20, 50, 10]}
                angle={0.12}
                penumbra={1}
                intensity={1}
                castShadow
                shadow-mapSize={1024}
            />
            <pointLight intensity={1} />
            {
                computer &&
                <primitive
                    object={computer?.scene}
                    scale={0.5}
                    position={[0, -3.25, -1.5]}
                />
            }

        </mesh>
    )
}

export default ComputeModel;