import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FaEye } from "react-icons/fa";
import { FaEyeSlash } from "react-icons/fa6";
import { hideViewObject } from '../../store/actions';

const Sidebar = () => {
    const selectedobject = useSelector((state: any) => state.ObjectReducer.selectedobject);
    const hideviewobject = useSelector((state: any) => state.SidebarReducer.hideviewobject);

    const dispatch = useDispatch();
    
    const handleHideViewObject = (state: 'hide' | 'view') => {
        dispatch(
            hideViewObject(
                state
            )
        );
    };

    return (
        <div className='flex flex-row justify-between mt-10 p-4'>
            <span className='text-semibold text-[16px] text-black'>{selectedobject?.name}</span>
            <span 
             className='text-black cursor-pointer' 
             onClick={() => handleHideViewObject(hideviewobject == 'hide' ? 'view' : 'hide')}
            >
                { hideviewobject == 'hide' ? <FaEye /> : <FaEyeSlash /> }
            </span>
        </div>
    )
};

export default Sidebar;