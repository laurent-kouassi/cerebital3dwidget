import React, { Suspense, useEffect, useState } from "react";
import { Canvas } from "@react-three/fiber";
import { OrbitControls, Preload, Stage } from "@react-three/drei";
import Loader from "./Loader";

const CanvasComponent = ({ children }: { children: React.ReactNode }) => {
    return (
        <Canvas
            frameloop="demand"
            shadows
            dpr={[1, 2]}
            camera={{ 
                position: [0.2, 0.1, 0.3], 
                fov: 25 
            }}
            gl={{ preserveDrawingBuffer: true }}
        >
            <Suspense fallback={<Loader />}>
                <OrbitControls
                    enableRotate={true}
                    enableZoom={true}
                    //maxPolarAngle={Math.PI / 2}
                    //nminPolarAngle={Math.PI / 2}
                    //dampingFactor={0.3}
                />
                {/* @ts-ignore */}
                <Stage contactShadow={{ resolution: 1024, scale: 1000 }}>
                    {children}
                </Stage>
            </Suspense>
            <Preload all />
        </Canvas>
    )
}

export default CanvasComponent;