const HIDE_VIEW_OBJECT: string = 'HIDE_VIEW_OBJECT';
const SELECTED_OBJECT: string = 'SELECTED_OBJECT';

const TYPES: any= {
    HIDE_VIEW_OBJECT,
    SELECTED_OBJECT
};

export default TYPES;