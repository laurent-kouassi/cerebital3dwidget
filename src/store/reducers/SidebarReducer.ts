import TYPES from "../types";

const initialState = {
    hideviewobject: 'view',
};

export const SidebarReducer = (state = initialState, action: { type: any; payload: { data: any; }; }) => {
  switch (action.type) {
    case TYPES.HIDE_VIEW_OBJECT:
      return {...state, hideviewobject: action.payload};
    default:
      return state;
  }
};
