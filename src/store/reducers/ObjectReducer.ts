import { objectsModelsData } from "../../components/Header/Header";
import TYPES from "../types";

const initialState = {
    selectedobject: objectsModelsData[1],
};

export const ObjectReducer = (state = initialState, action: { type: any; payload: { data: any; }; }) => {
  switch (action.type) {
    case TYPES.SELECTED_OBJECT:
      return {...state, selectedobject: action.payload};
    default:
      return state;
  }
};
