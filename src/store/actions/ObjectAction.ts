import TYPES from "../types";

export const selectedObject = (object: any) => {
    return async (dispatch: (arg0: { type: any; payload: any; }) => void) => {
        dispatch({
            type: TYPES.SELECTED_OBJECT,
            payload: object
        });
    };
};