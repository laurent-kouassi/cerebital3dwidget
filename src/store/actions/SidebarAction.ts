import TYPES from "../types";

export const hideViewObject = (state: any) => {
    return async (dispatch: (arg0: { type: any; payload: any; }) => void) => {
        dispatch({
            type: TYPES.HIDE_VIEW_OBJECT,
            payload: state
        });
    };
};