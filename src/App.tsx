import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from "react-redux";

import './App.css';

import store from './store';
import history from './store/history';
import { Routers } from './routers';
import Header from './components/Header/Header';


function App() {

  return (
    <Provider store={store}>
      <Header />
      {/* @ts-ignore */}
      <Router history={history}>
        <Routers />
      </Router>
    </Provider>

  );
};

export default App;
