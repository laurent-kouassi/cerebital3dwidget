import { Dashboard } from "../views";

const viewsUrls: any[] = [
    {
        path: "/",
        exact: true,
        component: Dashboard,
        type: "public"
    }
];

export default viewsUrls;