import React, { Suspense, useEffect } from "react";
import {
  Routes,
  Route,
  useLocation,
  useNavigate
} from 'react-router-dom';
import viewsUrls from "./routes";

function Routers(props: any) {

  return (
      <Routes>
        {
          viewsUrls?.map(({ component, path}: any) => {
            let Component = component;
            return (
              <Route 
                key={new Date().getDate()}
                path={path} 
                element = { <Component /> } 
              />
            );
          })
        }
      </Routes>
  );
}

export default Routers;
