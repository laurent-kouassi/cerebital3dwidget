import { FC, useEffect, useState } from "react";
// @ts-ignore
import Meteor from '@meteorrn/core';
import config from '../config.json';

// get internal info
Meteor.isVerbose = true

Meteor.connect(config.backend.url);
// console.log(config.backend.url);
// Meteor.connect('wss://atmospherejs.com/websocket');
// Meteor.connect("wss://atmospherejs.com/websocket");

interface useConnectionProps {

};

// handle auto-reconnect and updates state accordingly
const useConnection = () => {
    const [connected, setConnected] = useState<boolean | null>();
    const [connectionError, setConnectionError] = useState<any>();

    let status = Meteor.status();

    console.log({status});
    console.log({connected});
    useEffect(() => {
        const onError = (err: any) => setConnectionError(err);
        Meteor.ddp.on('error', onError);

        const onConnected = () => connected != true && setConnected(true);
        Meteor.ddp.on('connected', onConnected);
       
        // if connection lost switch state and force reconnection to server
        const onDisconnected = () => {
            Meteor.ddp.autoConnect = true;
            console.log({status});
            if (!connected) {
                console.log({connected});
                setConnected(false);
            };

            Meteor.reconnect();
        };
        Meteor.ddp.on('disconnected', onDisconnected);

        // clean listeners on unmounte
        return () => {
            Meteor.ddp.on('error', onError);
            Meteor.ddp.on('connected', onConnected);
            Meteor.ddp.on('disconnected', onDisconnected);
        };

    }, [connected]);

    return {
        connected,
        connectionError
    };
};

export default useConnection;