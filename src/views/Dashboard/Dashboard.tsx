import React from 'react';
import { 
  CanvasComponent, 
  ComputeModel, 
  Sidebar } from '../../components';
import { useSelector } from 'react-redux';

const Dashboard = () => {
  const hideviewobject = useSelector((state: any) => state.SidebarReducer.hideviewobject);
  
  return (
    <div className='flex flex-row'>
      <div className='bg-gray-200 w-[200px]'>
        <Sidebar />
      </div>
      <div id="canvas-container" style={{ width: "100vw", height: "90vh" }}>
        <CanvasComponent>
          {hideviewobject == 'view' && <ComputeModel />}
        </CanvasComponent>
      </div>
    </div>
  )
};

export default Dashboard;