

# CEREBITAL 3D OBJECT

## Features

- ReactJS
- TypeScript
- REDUX
- REDUX THUNK
- TailwindCSS
- THREE
- react-three

## Requirements

- [node & npm](https://nodejs.org/en/)

## Installation

- `git clone https://gitlab.com/laurent-kouassi/cerebital3dwidget`
- `cd cerebital3dwidget`
- `npm install --legacy-peer-deps`
- `npm start`


# TCEREBITAL 3D OBJECT