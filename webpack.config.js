const Dotenv = require('dotenv-webpack');
module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: "pre",
        use: ["source-map-loader"],
      },
    ],
  },
  resolve: {
    fallback: {
      util: require.resolve("util/"),
      https: require.resolve("https-browserify"),
      http: require.resolve("stream-http"),
      http: false,
      querystring: false
    }
  },
  ignoreWarnings: [/Failed to parse source map/],
  plugins: [
    new Dotenv()
  ]
};